from integers_utilities import gcd

class Fraction(object):

    def __init__(self, a, b):
        if b == 0:
            raise ValueError('Fraction denominator must be different to zero.')
        elif a == 0:
            self.num, self.den = 0, 1
        else:
            self.num, self.den = a, b

    def __str__(self):
        if self.num == 0:
            return '0'
        elif self.den == 1:
            return str(self.num)
        elif self.den == -1:
            return str(-self.num)
        else:
            return str(self.num) + "/" + str(self.den)

    def __repr__(self):
        return self.__str__()

    def __add__(self, other):
        return Fraction(self.num*other.den+self.den*other.num, self.den*other.den)

    def __sub__(self, other):
        return Fraction(self.num*other.den-self.den*other.num, self.den*other.den)

    def __mul__(self, other):
        return Fraction(self.num*other.num, self.den*other.den)

    def __truediv__(self, other):
        try:
            return Fraction(self.num*other.den, self.den*other.num)
        except ValueError:
            raise ValueError("can't do the division")

    def is_irreductible(self):
        return True if abs(gcd(self.num, self.den)) == 1 else False


    def __pow__(self, n):
        return Fraction(self.num**n, self.den**n)

    def __eq__(self, other):
        return ( self.num * other.den ==  self.den * other.num )

    def __ne__(self, other):
        return not (self == other)

    def __lt__(self, other):
        return self.num * other.den <  self.den * other.num

    def __le__(self, other):
        return self.num * other.den <=  self.den * other.num

    def __gt__(self, other):
        return self.num * other.den >  self.den * other.num

    def __ge__(self, other):
        return self.num * other.den >=  self.den * other.num

    def __neg__(self) :
        return Fraction(0,1) - self

    def __pos__(self) :
        return self

    def __isub__(self, other):
        self.num = self.num*other.num - self.den*other.num
        self.den = self.den*other.den

    def __iadd__(self, F):
        self.num =  self.num*F.num + self.den*other.num
        self.den =  self.den*other.den

    def __imul__(self, other):
        self.num *= other.num
        self.den *= other.den

    def __idiv__(self, other):
      if other.num == 0 :
        raise ValueError('error: division by zero')
      self.num *= other.den
      self.den *= other.num

# d = Fraction(545,0)
a = Fraction(1,3)
b = Fraction(2,7)

print( a == b )

print("b = ", b)
print(b.is_irreductible())

print(b.num, b.den)
print("b = ", b)

c = (a+b**2)*(b-a**4)
print(c)
c = (a+b**2)*(b-a**4+Fraction(12,1))
print(c)

print(gcd(2,7))
