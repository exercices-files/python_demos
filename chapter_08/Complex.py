from math import atan2

class ComplexNumber():

    def __init__(self, real = 0, imag = 0):
        self.SetReal(real)
        self.SetImag(imag)
        self.SetModulus()
        self.SetArgument()

    def SetReal(self, real):
        if not type(real) in {int, float}:
            raise TypeError('Real part of complex number must be real number')
        self.__real = real

    def SetImag(self, imag):
        if not type(imag) in {int, float}:
            raise TypeError('Imag part of complex number must be real number')
        self.__imag = imag

    def SetModulus(self):
        self.__modulus = (self.__real **2 + self.__imag **2)**(0.5)

    def SetArgument(self):
        self.__argument = atan2(self.__real, self.__imag)

    def GetReal(self):
        return self.__real

    def GetImag(self):
        return self.__imag

    def GetModulus(self):
        return self.__modulus

    def GetArgument(self):
        return self.__argument

    def __add__(self, other):
        if type(other) in (int, float):
            return  ComplexNumber(self.GetReal()+other, self.GetImag())
        return ComplexNumber(self.GetReal()+other.GetReal(),
                             self.GetImag()+other.GetImag())

    def __mul__(self, other):
        if type(other) in (int, float):
            return ComplexNumber(self.GetReal()*other, self.GetImag()*other)
        a, x = self.GetReal(), other.GetReal()
        b, y = self.GetImag(), other.GetImag()
        return ComplexNumber( a*x-b*y , a*y+b*x )

    def __truediv__(self, other):
        if type(other) in (int, float):
            return ComplexNumber(self.GetReal()/other, self.GetImag()/other)
        a, x = self.GetReal(), other.GetReal()
        b, y = self.GetImag(), other.GetImag()
        d = x**2+y**2
        return ComplexNumber((a*x+b*y)/d, (b*x-a*y)/d)

    def __repr__(self):
        return f"Cplx<=>({self.GetReal()}),({self.GetImag()})"

    def __str__(self):
        return f"Cplx<=>({self.GetReal()},{self.GetImag()})"

    def __eq__(self, other):
        return ( self.GetReal()  ==  other.GetReal() )          \
            and ( self.GetImag()  ==  other.GetImag() )

    def __ne__(self, other):
        return not (self == other)

    def __lt__(self, other):
        return ( self.GetReal() <  other.GetReal() )            \
            or (( self.GetReal()  ==  other.GetReal() )         \
                and (self.GetReal() <  other.GetReal()))

    def __le__(self, other):
        return ( self.GetReal() <=  other.GetReal() )           \
            or (( self.GetReal()  ==  other.GetReal() )         \
                and (self.GetReal() <=  other.GetReal()))

    def __gt__(self, other):
        return ( self.GetReal() >  other.GetReal() )            \
            or (( self.GetReal()  ==  other.GetReal() )         \
                and (self.GetReal() >  other.GetReal()))

    def __ge__(self, other):
        return ( self.GetReal() >=  other.GetReal() )           \
            or (( self.GetReal()  ==  other.GetReal() )         \
                and (self.GetReal() >=  other.GetReal()))

    def __neg__(self) :
        return ComplexNumber(-self.GetReal(), self.GetImag())

    def __pos__(self) :
        return self

    def __isub__(self, other):
        self.SetReal(self.GetReal()-other.GetReal())
        self.SetImag(self.GetImag()-other.GetImag())

    def __iadd__(self, other):
        self.SetReal(self.GetReal()+other.GetReal())
        self.SetImag(self.GetImag()+other.GetImag())

    def __imul__(self, other):
        if type(other) in (int, float):
            self.SetReal(other*self.GetReal())
            self.SetImag(other*self.GetImag())
        else:
            a, x = self.GetReal(), other.GetReal()
            b, y = self.GetImag(), other.GetImag()
            self.SetReal(a*x-b*y)
            self.SetImag(a*y+b*x)

    def __idiv__(self, other):
        if type(other) in (int, float):
            self.SetReal(self.GetReal()/other)
            self.SetImag(self.GetImag()/other)
        else:
            a, x = self.GetReal(), other.GetReal()
            b, y = self.GetImag(), other.GetImag()
            d = x**2+y**2
            self.SetReal((a*x+b*y)/d)
            self.SetImag((b*x-a*y)/d)


a = ComplexNumber(1,3)
b = ComplexNumber(7,2)
c = (a+b)*b
print(a)
print(b)
print(a+b)
print( (a*b+c)*(b+c) )
print(b/c)


print(a.GetModulus())

