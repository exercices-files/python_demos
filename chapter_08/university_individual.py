class Adresse(object):

	def __init__(self, postal, rue, ville):
		self.rue    = rue
		self.postal = postal
		self.ville  = ville

	def __repr__(self) -> str:
	    return self.rue + " " + str(self.postal)

	def __str__(self) -> str:
		return f""" rue : {self.rue}\n
				  ville : {self.ville}\n
				 postal : {self.postal}\n"""

	def __del__(self):
		print(f"""suppression de l'adresse {self.rue}
		      							   {self.ville}
		      							   {self.postal}""")


class Personne(object):

	def __init__(self, num, nom, prenom, adress):
		self.num     = num
		self.nom     = nom
		self.prenom  = prenom
		self.adresse = adress

	def __repr__(self) -> str:
	    return self.prenom + " " + self.nom

	def __str__(self) -> str:
		return f"""identifiant: {self.num}\n
		               prénom : {self.prenom}\n
		                  nom : {self.nom}\n
		            addresse  : {self.adresse.__repr__()}\n"""

	def __del__(self):
		print(f"suppression de la personne d'identifiant {self.num}")


class Etudiant(Personne):

	def __init__(self,  num, nom, prenom, adress, niveau, domain):
		super().__init__(num, nom, prenom, adress)
		self.niveau  = niveau
		self.domaine = domain

	def __repr__(self) -> str:
	    return self.num + " " + self.niveau

	def __str__(self) -> str:
		return f"""identifiant: {self.num}\n
		               niveau : {self.niveau}\n"""

	def __del__(self):
		print(f"""suppression de l'etudiant d'identifiant {self.num} -
		                                                  {self.niveau}""")


class Professeur(Personne):

	def __init__(self,  num, nom, prenom, adress, duree, domain):
		super().__init__(num, nom, prenom, adress)
		self.duree   = duree
		self.domaine = domain

	def __repr__(self) -> str:
	    return self.num + " " + self.duree

	def __str__(self) -> str:
		return f"""identifiant: {self.num}\n
		                duree : {self.duree}\n"""

	def __del__(self):
		print(f"""suppression du professeur d'identifiant {self.num} -
		                                                  {self.domaine}""")


x = Adresse(75018, "belliard", "paris")
y = Adresse(13003, "Pyat", "marseille")
a = Personne(100, "Name", "Surname", x)
b = Personne(101, "Nom", "Prenom", y)

c = Etudiant(1234567, "Student", "Not Learning", x, "Licence 1", "Sociologie")
d = Professeur(9999999, "Student", "Not Learning", x, 25, "Mathématiques")

print(a)
print(c)
print(d)
