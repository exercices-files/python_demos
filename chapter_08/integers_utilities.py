def gcd(a,b):

    a = max(abs(a), abs(b))
    b = min(abs(b), abs(a))

    def aux_gcd(a,b):
        if a % b == 0:
            return b
        return aux_gcd(b, a % b)

    try:
        if (0 == b):
            if (0 != a):
                return a
            else:
                raise ValueError
    except ValueError:
        raise ValueError('gcd error value: a * b = 0')

    return aux_gcd(a, b)


def bezout(a,b):
    ''' return couple of integers (u,v) such as
        a u + b v = gcd(a,b) '''

    if b==0:
        return (1,0)
    else:
        u, v = bezout(b, a % b)
        return (v, u-(a//b)*v)

def inv(a,p):
    return (a, p)[0] % p

def binom(n,k):
    ''' binomial coefficient '''

    p = 1
    for i in range(k):
        p = (p * (n-i)) // (i+1)
    return p

def binom_mod(n,k,p):
    ''' binomial coefficient modulo prime interger p '''

    p = 1
    for i in range(k):
        p = (p * (n-i) * inv(i+1,p) % (i+1))
    return p
