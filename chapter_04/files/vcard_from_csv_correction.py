def csv2vcard(csv_filename, vcard_filename):

    in_file = open(csv_filename, mode='r')
    out_file = open(vcard_filename, mode='w')

    newentry = """BEGIN:VCARD\nVERSION:3.0\nN:%s;%s\n
                  FN:%s\nTEL;TYPE=HOME;VOICE:%s\n
                  EMAIL;TYPE=PREF,INTERNET:%s\nEND:VCARD\n"""

    for line in in_file:
        pos = line.split(',')
        out_file.write(newentry %  (pos[0], pos[1], pos[4], pos[7], pos[6]) )

    in_file.close()
    out_file.close()

csv2vcard("contacts_gmail.csv", "contacts_gmail.vcard")
