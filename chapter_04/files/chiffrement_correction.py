def decalage(lettre, pas_de_decalage):
    oldLetterPosition = ord(lettre)
    newLetterPosition = (oldLetterPosition + pas_de_decalage ) % 0x110000
    newLetter = chr(newLetterPosition)
    return newLetter

def cryptage(fichierClair, fichierCrypte, cle_decalage):
    fichierSource = open(fichierClair, mode='r', encoding='utf-8')
    fichierDestin = open(fichierCrypte, mode ='w', encoding='utf-8')

    for ligne in fichierSource:
        for caractere in ligne:
            newCaractere = decalage(caractere, cle_decalage)
            fichierDestin.write(newCaractere)

    fichierSource.close()
    fichierDestin.close()

offset = 123132123130

cryptage("gnu_licence.txt", "gnu_licence.crypted", offset)
cryptage("gnu_licence.crypted","original.txt" ,-offset)
