from os.path import exists

def file_duplication(src, dst=None):

    dst = src + " (copy)" if dst is None else dst

    if not exists(src):
        print('source file does no exist')

    f_src = open(src, mode='r', encoding='utf-8')
    f_dst = open(dst, mode='w', encoding='utf-8')

    for line in f_src:
        f_dst.write(line)
    f_src.close()
    f_dst.close()
    print('all done fine')
    return None


# quelques test pour vérifier le bon fonctionnement

args = [ 'src="src_file.txt"   , dst="dst_file.txt"' , \
         'src=" src_file.txt"  , dst="dst_file.txt"' , \
         'src="src_file.txt"   , dst="dst_file.txt"' , \
         'src="src_file.txt" ' ,                       \
         'src=" src_file.txt"' ,                       \
         'src="src_file.txt"   , dst="dst_file.txt"' ]

for arg in args:
    eval( "file_duplication(" + arg + ")" )
