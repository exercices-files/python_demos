"""
    construction d'un histogramme à partir des suffrages
"""
nbVoix = "besancenot 1498581 buffet 707268 schivardi 123540 bayrou 6820119 bové 483008 voynet 576666 villiers 818407 royal 9500112 dom 0 nihous 420645 le pen 3834530 laguiller 487857 sarkozy 11448663"

liste = nbVoix.split(" ")

# gestion des noms composés et/ou avec particules non numériques
i, longueur = 0, len(liste)
while i < longueur:
    if not (liste[i].isdigit() or liste[i+1].isdigit()):
        liste[i] += " " + liste[i+1]
        liste.remove(liste[i+1])
        longueur -= 1
        i -= 1
    i += 1

# méthode 1
nbVoixParCandidat = dict( (liste[i*2],int(liste[2*i+1])) for i in  range(len(liste)//2) )

# méthode 2
nbVoixParCandidat = dict( (liste[i],int(liste[i+1])) for i in  range(0,len(liste),2) )

# méthode 3
liste1 = liste[0::2]
liste2 = liste[1::2]
nbVoixParCandidat = dict( (liste1[i],int(liste2[i])) for i in  range(len(liste1)) )

def pourcentage(dico) :
    # méthode 1
    s = sum(dico[k] for k in dico.keys())

    # méthode 2
    s = sum(dico.values())


    # methode 1
    dico2 = { k : dico[k]*100/s for k in dico.keys() }

    # methode 2
    # for k in dico.keys():
    #     pass

    return dico2



def histo(dico) :
    d = pourcentage(dico)
    l = max(len(k) for k in d.keys())
    print("histogramme (classement alphabetique) :")
    for k in sorted(d.keys()) :
        print(("%-"+str(l)+"s (%7.4f %%): %s") % ((k), d[k], "*"*round(2*d[k])))

    print("\n\nhistogramme (classement numérique inverse) :")
    D = {k: v for k, v in sorted(d.items(), key=lambda item: item[1], reverse=True)}
    for k in D :
        print(("%-"+str(l)+"s (%7.4f %%): %s") % ((k), D[k], "*"*round(2*D[k])))



print("nombre de votes par candidat (dictionnaire): \n", nbVoixParCandidat, "")
print("pourcentage par candidat (dictionnaire): \n", pourcentage(nbVoixParCandidat), "\n")
histo(nbVoixParCandidat)
