#  some tests
print(resolution(1,'2',1))
print(resolution(1,2,1))
print(resolution(1,2,-1))
print(resolution(0,0,0))
print(resolution(1,2,0))
print(resolution(0,2,1))
print(resolution(1,0,6))
print(resolution(1,1,0))
print(resolution(0,0,6))
print(resolution(1,0,0))
print(resolution(0,1,0))


        # expected results
        "incorrect input"
        [1, {-1.0}]
        [2, {0.41421356237309515, -2.414213562373095}]
        "any real number x is solution"
        [2, {0, -2.0}]
        [1, {-0.5}]
        [2, {2.449489742783178, -2.449489742783178}]
        [2, {0, -1.0}]
        [0, {}]
        [1, {0.0}]
        [0, {}]