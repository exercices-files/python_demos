""" second order polynomial equation solving
    will be needed for code coverage course """

def resolution(a, b, c):
    """ second order polynomial equation solving """

    if not {type(i) for i in [a, b, c]} <= {int, float}:
        print("incorrect input ")
        res = None
    else:
        if a*b*c != 0:
            delta = b**2-4*a*c
            if delta < 0:
                res = [0, {}]
            elif delta == 0:
                res = [1, {-b/(2*a)}]
            else:
                sqrt = delta**(0.5)
                res = [2, {(-b-sqrt)/(2*a), (-b+sqrt)/(2*a)}]
        else:
            if {a, b, c} <= {0., 0}:
                print('any real number x is solution')
                res = None
            elif b*c != 0:
                res = [1, {-c/b}]
            elif a*b != 0:
                res = [2, {0, -b/a}]
            else:
                if a == 0:
                    res = [0, {}]
                elif a*c < 0:
                    res = [0, {}]
                else:
                    sqrt = (c/a)**(0.5)
                    if sqrt == 0:
                        res = [1, {sqrt, -sqrt}]
                    else:
                        res = [2, {sqrt, -sqrt}]
    return res

