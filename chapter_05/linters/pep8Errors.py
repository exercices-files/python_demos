letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'                  # pep8 utilization: pep8Errors.py

def main():
    message = input('Enter message: ')
    key = input('Enter key [alphanumeric]: ')
    mode = input('Encrypt/Decrypt [e/d]: ')

    if mode.lower().startswith('e'):
        mode, translated = 'encrypt', encryptMessage(key, message)
    elif mode.lower().startswith('d'):
        mode, translated = 'decrypt', decryptMessage(key, message)
    print('\n%sed message:\n' % mode.title(), translated)

def encryptMessage(key, message):  return translateMessage(key, message, 'encrypt')

def decryptMessage(key, message):  return translateMessage(key, message, 'decrypt')

def translateMessage(key, message, mode):
    translated, keyIndex, key = [], 0, key.upper()
    for symbol in message:
        num = letters.find(symbol.upper())
        if num != -1:
            if mode == 'encrypt':
                num += letters.find(key[keyIndex])
            elif mode == 'decrypt':
                num -= letters.find(key[keyIndex])
            num %= len(letters)

            if symbol.isupper(): translated.append(letters[num])
            elif symbol.islower(): translated.append(letters[num].lower())

            keyIndex += 1
            if keyIndex == len(key): keyIndex = 0
        else: translated.append(symbol)
    return ''.join(translated)

if __name__ == '__main__':
    main()
