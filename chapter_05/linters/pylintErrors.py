import random, os, re         # pylint8 utilization: pylintErrors.py

def rabinMiller(num):
    s, t = num - 1, 0
    while s % 2 == 0:
        s = s // 2
        t += 1
    for trials in range(5):
        a = random.randrange(2, num - 1)
        v = pow(a, s, num)
        if v != 1:
            i = 0
            while v != (num - 1):
                if i == t - 1: return False
                else:
                    i = i + 1
                    v = (v ** 2) % num

    return True

def isPrime(num):
    if (num < 2): return False
    from rabbinmiller_aux import lowPrimes
    if num in lowPrimes: return True
    for prime in lowPrimes:
        if (num % prime) == 0: return False
    return rabinMiller(num)

def generateLargePrime(keysize = 1024):
    while True:
        num = random.randrange(2 ** (keysize-1), 2 ** (keysize))
        if isPrime(num):
            return num

if __name__ == '__main__':
    num = generateLargePrime()
    print('Prime number:', num, '\n isPrime: ', isPrime(num))
