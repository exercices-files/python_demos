"""
    implementation of the Rabbin-Miller algorithm for pseudo primality test

    date:       22.11.2021
    author:     ibrahim.ms@gmail.com
    filename:   pylint_corrections.py
    purpose:    pep8 and pylint utilization from pylint8Errors.py

"""

import random

from rabbin_miller_aux import LOW_PRIMES


def rabin_miller(num):
    """
    { function_description }

    :param      num:  The number
    :type       num:  { type_description }

    :returns:   { description_of_the_return_value }
    :rtype:     { return_type_description }
    """
    int_s, int_t = num - 1, 0
    while int_s % 2 == 0:
        int_s = int_s // 2
        int_t += 1
    for _ in range(5):
        int_a = random.randrange(2, num - 1)
        int_v = pow(int_a, int_s, num)
        if int_v != 1:
            i = 0
            while int_v != (num - 1):
                if i == int_t - 1:
                    return False
                else:
                    i = i + 1
                    int_v = (int_v ** 2) % num

    return True


def is_prime(num):
    """
    Determines whether the specified number is prime.

    :param      num:  The number
    :type       num:  { type_description }

    :returns:   True if the specified number is prime, False otherwise.
    :rtype:     bool
    """
    if num < 2:
        return False
    if num in LOW_PRIMES:
        return True
    for prime in LOW_PRIMES:
        if (num % prime) == 0:
            return False
    return rabin_miller(num)


def generate_large_number(keysize=1024):
    """
    { function_description }

    :param      keysize:  The keysize
    :type       keysize:  int

    :returns:   { description_of_the_return_value }
    :rtype:     { return_type_description }
    """
    while True:
        some_number = random.randrange(2 ** (keysize-1), 2 ** (keysize))
        if is_prime(some_number):
            return some_number


if __name__ == '__main__':
    number = generate_large_number()
    print('Prime number:', number, '\n isPrime: ', is_prime(number))
