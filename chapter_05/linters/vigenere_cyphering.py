"""
    implementation of the vigenere de-cyphering algorithm

    date:       22.11.2021
    author:     ibrahim.ms@gmail.com
    filename:   vigenere_cyphering.py
    purpose:    pep8 and pylint utilization from pep8Errors.py

"""

THE_LETTERS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'


def main():
    """
    Produces the crypted and uncrypted files from the original
    in the same directory

    :returns:   Nothing
    :rtype:     None
    """
    message = input('Enter message: ')
    key = input('Enter key [alphanumeric]: ')
    mode = input('Encrypt/Decrypt [e/d]: ')

    if mode.lower().startswith('e'):
        mode, translated = 'encrypt', encryp_message(key, message)
    elif mode.lower().startswith('d'):
        mode, translated = 'decrypt', decrypt_message(key, message)
    print('\n%sed message:\n' % mode.title(), translated)


def encryp_message(key, message):
    """
    apply calculations to transform message to its coded version

    :param      key:      alphanumeric key for cyphering
    :type       key:      string
    :param      message:  the message string
    :type       message:  string

    :returns:   coded message representation
    :rtype:     string
    """
    return translate_message(key, message, 'encrypt')


def decrypt_message(key, message):
    """
    apply calculations to transform coded message to its clear version

    :param      key:      alphanumeric key for decyphering
    :type       key:      string
    :param      message:  the coded message string
    :type       key:      string

    :returns:   clear message representation
    :type:      string
    """
    return translate_message(key, message, 'decrypt')


def translate_message(key, message, mode):
    """
    { function_description }

    :param      key:      The key
    :type       key:      string
    :param      message:  clear or coded message representation
    :type       message:  string
    :param      mode:     'encrypt' for cyphering  'decrypt' otherwise
    :type       mode:     string

    :returns:   clear or coded message representation
    :rtype:     string
    """
    translated, key_index, key = [], 0, key.upper()
    for symbol in message:
        num = THE_LETTERS.find(symbol.upper())
        if num != -1:
            if mode == 'encrypt':
                num += THE_LETTERS.find(key[key_index])
            elif mode == 'decrypt':
                num -= THE_LETTERS.find(key[key_index])
            num %= len(THE_LETTERS)

            if symbol.isupper():
                translated.append(THE_LETTERS[num])
            elif symbol.islower():
                translated.append(THE_LETTERS[num].lower())

            key_index += 1
            if key_index == len(key):
                key_index = 0
        else:
            translated.append(symbol)
    return ''.join(translated)


if __name__ == '__main__':
    main()
