#!/usr/bin/env python3
# coding: utf-8

"""
   Calcul une approximation stochastique du nombre pi
   par la méthode de Bernoulli (sauf erreur, à vérifier)

   Utilisation de fonctions et de sous fonctions (perso ou std)
               de matplotlib pour la représentation graphique
               du module random pour une fonction d'aléa uniforme
"""

import matplotlib.pyplot as pyplot
from math import sqrt
import random

def demiCercleSup(x,x0,y0,r):
    """ Calcul des coordonnées du demi cercle supérieur
        centré en x0 y0 et de rayon r """
    return y0+sqrt(r*r-(x-x0)**2)

def demiCercleInf(x,x0,y0,r):
    """ Calcul des coordonnées du demi cercle inférieur
        centré en x0 y0 et de rayon r """
    return y0-sqrt(r*r-(x-x0)**2)

def tracerCercleEtPoints(n,x0,y0,r):
    """Trace un cercle de centre (x0,y0) et de rayon r avec n points
       tirés aléatoirement dans le carré centré en (x0,y0) et de coté 2*r
    """
    x=[x0-r+i*r*2/(n-1) for i in range(n)]
    y1=[demiCercleSup(x[i],x0,y0,r) for i in range(n)]
    y2=[demiCercleInf(x[i],x0,y0,r) for i in range(n)]
    pyplot.plot(x, y1, 'k')
    pyplot.plot(x, y2, 'k')
    pyplot.vlines(x0,x0-r,x0+r)
    pyplot.hlines(y0,y0-r,y0+r)

def tracerpoints(n,x0,y0,r):
    """
       Trace un cercle de centre (x0,y0) et de rayon r avec n points
       tirés aléatoirement dans le carré centré en (x0,y0) et de coté 2*r .
       Points intérieurs au cercle sont en vert.
       Points extérieurs au cercle sont en rouge.
       Points sur les cercles sont marqués par un croix noire.
    """
    tracerCercleEtPoints(n,x0,y0,r)
    x=[random.uniform(x0-r,x0+r) for i in range(n)]
    y=[random.uniform(y0-r,y0+r) for i in range(n)]
    for i in range(n):
        if y[i] in [demiCercleInf(x[i],x0,y0,r),demiCercleSup(x[i],x0,y0,r)]:
        # point sur le cercle
            pyplot.plot(x[i],y[i],'kx')
        elif demiCercleInf(x[i],x0,y0,r) < y[i] < demiCercleSup(x[i],x0,y0,r):
        # point intérieur
            pyplot.plot(x[i],y[i],'go')
        else:
            pyplot.plot(x[i],y[i],'ro')
    return x,y

def calculpi(nbTirages,x0,y0,r):
    x,y = tracerpoints(nbTirages,x0,y0,r)
    nbPointsInterieurs = 0
    for i in range(nbTirages):
        if demiCercleInf(x[i],x0,y0,r) < y[i] < demiCercleSup(x[i],x0,y0,r):
            nbPointsInterieurs += 1
    approx = float(nbPointsInterieurs)/nbTirages*4
    print('approx pi = %1.7f  with %9d runs'%(approx, nbTirages))
    pyplot.title('\u03C0 \u2248 %1.7f' % (approx), fontsize=30)
    pyplot.gca().set_aspect('equal')
    pyplot.show(block=False)
    pyplot.pause(1)
    pyplot.close()

for nbTirages in [100*i for i in range(5,35)]:
  calculpi(nbTirages,0,0,1)

