import matplotlib.pyplot as pyplot                   # graphics.py
import random
from calculations import demiCercleInf, demiCercleSup
def tracerCercleEtPoints(n,x0,y0,r):
    x=[x0-r+i*r*2/(n-1) for i in range(n)]
    y1=[demiCercleSup(x[i],x0,y0,r) for i in range(n)]
    y2=[demiCercleInf(x[i],x0,y0,r) for i in range(n)]
    pyplot.plot(x, y1, 'k')
    pyplot.plot(x, y2, 'k')
    pyplot.vlines(x0,x0-r,x0+r)
    pyplot.hlines(y0,y0-r,y0+r)

def tracerpoints(n,x0,y0,r):
    tracerCercleEtPoints(n,x0,y0,r)
    x=[random.uniform(x0-r,x0+r) for i in range(n)]
    y=[random.uniform(y0-r,y0+r) for i in range(n)]
    for i in range(n):
        if y[i] in [demiCercleInf(x[i],x0,y0,r),demiCercleSup(x[i],x0,y0,r)]:
        # point sur le cercle
            pyplot.plot(x[i],y[i],'kx')
        elif demiCercleInf(x[i],x0,y0,r) < y[i] < demiCercleSup(x[i],x0,y0,r):
        # point intérieur
            pyplot.plot(x[i],y[i],'go')
        else:
            pyplot.plot(x[i],y[i],'ro')
    return x,y
