import matplotlib.pyplot as pyplot                     # approx_numbers.py
from graphics import tracerpoints
from calculations import demiCercleInf, demiCercleSup

def approx_pi(nbTirages,x0,y0,r):
    x,y = tracerpoints(nbTirages,x0,y0,r)
    nbPointsInterieurs = 0
    for i in range(nbTirages):
        if demiCercleInf(x[i],x0,y0,r) < y[i] < demiCercleSup(x[i],x0,y0,r):
            nbPointsInterieurs += 1
    approx = float(nbPointsInterieurs)/nbTirages*4
    print('approx pi = %1.7f  with %9d runs'%(approx, nbTirages))
    pyplot.title('\u03C0 \u2248 %1.7f' % (approx), fontsize=30)
    pyplot.gca().set_aspect('equal')
    pyplot.show(block=False)
    pyplot.pause(1)
    pyplot.close()
