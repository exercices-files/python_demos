from math import sqrt                       # calculations.py

def demiCercleSup(x,x0,y0,r):
    return y0+sqrt(r*r-(x-x0)**2)

def demiCercleInf(x,x0,y0,r):
    return y0-sqrt(r*r-(x-x0)**2)
