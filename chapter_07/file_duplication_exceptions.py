    """ exception exercice:
      file duplication
      examplifying the eval/exec functions
"""
from os.path import exists

def duplication(**args):
    """ two named parameters are mandatory
        src: the file to copy
        dst: the file to create

        three named parameters are optionnal
          overwrite: overwite the dst file
          append: append to the dst file
          verbose:   default is False

     """
    if not {'src', 'dst'} <= args.keys():
        raise TypeError("missing argument: src et dst are mandatory")
    src, dst = args['src'], args['dst']
    append = args['append'] if 'append' in args.keys() else False
    overwrite = args['overwrite'] if 'overwrite' in args.keys() else False
    verbose = args['verbose'] if 'verbose' in args.keys() else True

    # for verification purpose in verbose mode only
    if verbose:
        print()
        print("src       = ", src)
        print("dst       = ", dst)
        print("overwrite = ", overwrite)
        print("append    = ", append)

    mode_dst = 'a' if append else 'w'

    try:
        if not exists(src):
            raise FileNotFoundError('source file does no exist')

        elif append and overwrite:
            raise ValueError('cannot do both append and overwrite')

        elif not exists(dst) and overwrite:
            raise FileNotFoundError('cannot overwrite non existing file')

        elif not exists(dst) and append:
            raise FileNotFoundError('cannot append to non existing file')


        f_src = open(src, mode='r', encoding='utf-8')
        f_dst = open(dst, mode=mode_dst, encoding='utf-8')

        for line in f_src:
            f_dst.write(line)

        f_src.close()
        f_dst.close()

        if verbose:
            print('no error: all done ')

    except FileNotFoundError as error:
        print("error of file: {} ".format(error))

    except ValueError as error:
        print("usage error: {} ".format(error))


# quelques test pour vérification
for arguments in [
    ' src = "src_file.txt"  , dst = "dst_file.txt   ,                                       verbose = True"' ,  \
    ' src = " src_file.txt" , dst = "dst_file.txt"  ,                                       verbose = True'  ,  \
    ' src = "src_file.txt"  , dst = " dst_file.txt" ,    append = True ,                    verbose = True'  ,  \
    ' src = "src_file.txt"  , dst = " dst_file.txt" ,    append = True , overwrite = True , verbose = True'  ,  \
    ' src = "src_file.txt"  , dst = " dst_file.txt" , overwrite = True ,                    verbose = True'  ,  \
    ' src = "src_file.txt"  , dst = "dst_file.txt"  ,    append = True , overwrite = True , verbose = True'  ,  \
    ' src = "src_file.txt"  , dst = " dst_file.txt" ,   append  = True , overwrite = True , verbose = True'  ,  ]:
    exec('duplication('+ arguments +')')
    #eval('duplication('+ arguments +')')
