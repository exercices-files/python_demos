#! /usr/bin/env python3
from sys import argv
from os.path import exists, isdir

def counting(filename):

    if exists(filename):
        if isdir(filename):
            what_done = "(***) " + filename + " skipped because it's a directory (***)\n"
        else:
            n_l = n_w = n_c = 0
            with open(filename, 'r') as my_file:
                for line in my_file:
                    n_l += 1
                    n_c += len(line)
                    n_w += len(line.split())
            what_done = "in %s\n %d lines %d words %d chars\n" % (filename, n_l, n_w, n_c)
    else:
        what_done = "(+++) not found: %s (+++)\n" % filename
    return what_done

if len(argv) > 1:
    list_of_files = argv[1:]
else:
    print("no argument given")
    exit(0)

for filename in list_of_files: print(counting(filename))
