#!/usr/bin/env python3
"""  usage : unzip unFichierZippé.zip
    Extrait les fichiers et les sauvegarde dans des sous-répertoires par type d'extension. """

# import des modules et fonctions python dont nous avons besoin
from sys import argv as arguments
from os.path import exists, isdir
from os import mkdir
from shutil import rmtree
import zipfile

# s'il n'y a pas exactement deux elements qui composent la ligne de commande, c'est une erreur
if len(arguments) != 2:
    print("\tunzip_by_ext requiert exactement un argument:\n\t\tunzip_by_ext fichier.zip\n")
    exit(1)

ZIPFILENAME = arguments[1]
# plus besoin d'afficher le nom du fichier
# print('le fichier à manipuler est : ', ZIPFILENAME)

 #ce n'est pas un fichier zip : test insufisant -- mais pour un début, c'est bon
if not ZIPFILENAME.endswith('.zip'):
    print("le fichier : ", ZIPFILENAME, "n'est pas un fichier zip")
    exit(2)

#  le fichier en argument n'existe pas
elif not exists(ZIPFILENAME):
    print(ZIPFILENAME, "n'est pas un fichier existant")
    exit(3)

#le programme de manipulation des fichiers zip a échoué à ouvrir le fichier en argument
elif not zipfile.is_zipfile(ZIPFILENAME):
    print("l'archive : ", ZIPFILENAME, "est non reconnue comme intégre")
    exit(4)

else:
    # on peut traiter le fichier et extraire les fichiers
    DIRECTORY_NAME = ZIPFILENAME[:-4]
    with zipfile.ZipFile(ZIPFILENAME, 'r') as ZIP_FILE_OBJECT:

        # supprimer le répertoire destination s'il existe
        if isdir(DIRECTORY_NAME):
            rmtree(DIRECTORY_NAME)

        # avant de pouvoir le créer
        mkdir(DIRECTORY_NAME)

        # plus besoin d'afficher ceci
        # print("le repertoire principal : ", DIRECTORY_NAME, " a été créé")

        LIST_OF_FILES = ZIP_FILE_OBJECT.filelist

        DICO_EXT = {}

        for FICHIER in LIST_OF_FILES:

            # le nom est le dernier élément du chemin absolu
            # print(FICHIER.filename)
            FICHIER_NAME = FICHIER.filename.split('/')[-1]

            # plus besoin d'afficher cela
            # print(FICHIER.filename)
            # print(FICHIER.filename.split('/'))

            # fichier courant : répertoire, passer au suivant sans rien faire
            if FICHIER_NAME == '':
                continue
            # on a faire à un fichier régulier
            else:
                L = FICHIER_NAME.split('.')
                if len(L) == 1:
                    # pas de point dans le nom : fichier sans extension
                    EXTENSION = 'withNoExtensionFiles'
                else:
                    # l'extension est ce qui suit le dernier .
                    EXTENSION = L[-1]

                DESTINATION = DIRECTORY_NAME+'/'+EXTENSION

                # si le répertoire destination existe déja, alors : pas besoin de le créer
                if not isdir(DESTINATION):
                    mkdir(DESTINATION)
                    DICO_EXT[EXTENSION] = 1
                else:
                    DICO_EXT[EXTENSION] += 1

                # le fichier à créer est un fichier binaire
                f = open(DESTINATION+'/'+FICHIER_NAME, mode='wb')

                # récuperer le contenu du fichier courant depuis l'archive
                f.write(ZIP_FILE_OBJECT.read(FICHIER.filename))

                # ne pas oublier la fermeture du fichier
                f.close()

    # ouverture en écriture du fichier de log
    report_name = ZIPFILENAME + " (report).log"
    report = open(report_name, 'w')

    for extension in DICO_EXT:
        ext = " %+3s file(s) of type:  %s\n" % (str(DICO_EXT[extension]), extension)
        # affichage à l'écran
        print(ext[:-1])

        # écriture du fichier de log
        report.write(ext)

    # fermeture du fichier de log
    report.close()

    # code de retour de fonction: tout s'est bien passé
    exit(0)
