n = 26
tous              = [(x, y, z) for x in range(n) for y in range(n) for z in range(n) ]
distincs          = [(x, y, z) for x in range(n) for y in range(n) for z in range(n) if len(set((x,y,z)))==3]
croissants        = [(x, y, z) for x in range(n) for y in range(n) for z in range(n) if x<y<z]
Croissants = [(x, y, z) for x in range(n) for y in range(n) for z in range(n) if x<=y<=z]

proportion_1 = len(distincs)/len(tous)*100
proportion_2 = len(croissants)/len(tous)*100
proportion_3 = len(Croissants)/len(tous)*100

print("La proportion P1 est de : %2.2f %%." %  proportion_1)
print("La proportion P2 est de : %2.2f %%." %  proportion_2)
print("La proportion P3 est de : %2.2f %%." %  proportion_3)
