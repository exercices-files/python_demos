KasparovKarpov = "1. e4 e5 2. Nf3 Nc6 3. Bb5 a6 4. Ba4 Nf6 5. O-O Be7 6. Re1 b5 7. Bb3 d6 8. c3 O-O 9. h3 Bb7 10. d4 Re8 11. Nbd2 Bf8 12. a4 h6 13. Bc2 ed4 14. cd4 Nb4 15. Bb1 c5 16. d5 Nd7 17. Ra3 c4 18. Nd4 Qf6 19. N2f3 Nc5 20. ab5 ab5 21. Nb5 Ra3 22. Na3 Ba6 23. Re3 Rb8 24. e5 de5 25. Ne5 Nbd3 26. Ng4 Qb6 27. Rg3 g6 28. Bh6 Qb2 29. Qf3 Nd7 30. Bf8 Kf8 31. Kh2 Rb3 32. Bd3 cd3 33. Qf4 Qa3 34. Nh6 Qe7 35. Rg6 Qe5 36. Rg8 Ke7 37. d6 Ke6 38. Re8 Kd5 39. Re5 Ne5 40. d7 Rb8 41. Nf7"

# Kasparov plays in two steps
step1 = KasparovKarpov.split(' ')
Kasparov = step1[1 : : 3]

# Kasparov plays in one step
Kasparov = KasparovKarpov.split(' ')[1 : : 3]


# Karpov plays in two steps
step2 = KasparovKarpov.split(' ')
Karpov = step2[2 : : 3]

# Karpov plays in one step
Karpov = KasparovKarpov.split(' ')[2 : : 3]



print('les ', len(Kasparov), ' coups de Kasparov sont :\n  ', Kasparov)
print('les ', len(Karpov), ' coups de Karpov sont :\n  ', Karpov)


coups = KasparovKarpov.split(' ')

# trois méthodes différentes
spuoc1 = ([ coups[i] for i in range(len(coups)) if i % 3 != 0])[ ::-1]

spuoc2 = ([ i for i in coups if not '.' in i])[ ::-1]

spuoc3 = ([ i for i in coups if not (i[:-1]).isdigit() ])[ ::-1]


print('Les coups joués du dernier au premier sont :\n ', spuoc1)

print('Les coups joués du dernier au premier sont :\n ', spuoc2)


# verification d'égalité des résultats des différentes méthodes
print(spuoc1 == spuoc2)
print(spuoc1 == spuoc3)
