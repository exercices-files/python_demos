from gnuText import gnu as texte

print("Le texte contient %d caractères sur un alphabet de %d symboles.\n" % (len(texte), len(set(texte))))

dico = dict( (c, texte.count(c) ) for c in set(texte))

li_occ = list( (c, dico[c] ) for c in dico.keys())
print(li_occ)
# for i in li_occ:
#   print(i[0], "  ", i[1])

# Le résultat suivant est faux car ce critère de tri est l'ordre
# ascii sur les clefs (les symboles composants le texte)
li_occ.sort( reverse=True)
print(li_occ)

# Il faut spécifier le critère de tris
li_occ.sort(key=lambda b :b[1] , reverse=True)
print(li_occ)

# for i in li_occ:
#   print(i[0], "  ", i[1])
