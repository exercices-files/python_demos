# equivalence entre construction en intention/comprehension et iteration classique

a = [1,4,7,6]

b = [0,2,4,6]

c = {"1","5","10","15","20","25","0"}



[x for x in a if x > 5]
l = []
for i in a:
    if i > 5 :
        l.append(i)

l == [x for x in a if x > 5]




[2**x for x in b]
l = []
for i in range(len(b)):
    l.append(2**b[i])

l == [2**x for x in b]




[str(int(x)*10) for x in c]
l = []
for i in c:
    res = str(int(i)*10)
    l.append(res)

l == [str(int(x)*10) for x in c]            # can be false due to elements ordering
set(l) == set([str(int(x)*10) for x in c])  # always true
