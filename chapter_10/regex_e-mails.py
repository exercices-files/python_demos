import re
# from histoElection import pourcentage

regex_domain = re.compile(r'@([a-zA-Z0-9_\-\.]+)')

with open('regex_e-mails.txt', mode='r') as contacts_file:
    l_doms = re.findall(regex_domain, contacts_file.read())

d_doms, d_doms_percent, s = {}, {}, len(l_doms)

for dom in l_doms:
    if dom in d_doms:
        d_doms[dom] += 1
    else:
        d_doms[dom] = 1

for k in d_doms:
    d_doms_percent[k] = d_doms[k] * 100 / s


l_max = len(max(d_doms_percent.keys(), key=len))
tmplt = '{:<' + str(l_max) + '} [{:>5} % ] : '

for k in sorted(d_doms_percent.keys()):
    print(tmplt.format(k, round(d_doms_percent[k], 2)), "*" * int(d_doms_percent[k] + 1))

# commentaire:  on peut penser que ce fichier n'est pas réaliste
#               et que ces data ont été générées par un programme
