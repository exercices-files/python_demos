import re

text = """
Hello to you,
My office number is 415-730-0000.
Call me for any question about this course.

And don't call me for any other reason
- Me: 415-730-0000, +1-202-555-0123.

some random numbers:
 Local (Annapolis) 410-555-O170   410-555-0132   410-555-0141   410-555-0177   410-555-0141   1!0-555-0182

 International (Annapolis) +1-410-555-0170   +1-410-555-0132 +1-410-555-0141   +1-410-5 5-0177    +1-410-555-0141   +1-410-555-0182

 Local (US) 202-555-0129   202-555-0171 202-555-0168   202-555-0134 202-555-0138   202-555-0118     +1-202-555-011fsqgf8

 International (US) +1-202-555-0129   +1-20B-555-0171 +1-202-555-0168   +1-202-555-0123 +1-202-555-0138   +1-202-555-0118   +1-202-555-01212121218
"""

def isPhoneNumber(text):
    if len(text) != 12:
        return False
    for i in range(0, 3):
        if not text[i].isdecimal():
            return False
    if text[3] != '-':
        return False
    for i in range(4, 7):
        if not text[i].isdecimal():
            return False
    if text[7] != '-':
        return False
    for i in range(8, 12):
        if not text[i].isdecimal():
            return False
    return True

k = 0
for i, _ in enumerate(text):
    if isPhoneNumber(text[i:i+12]):
        k += 1
        print(text[i:i+12], end = ",  ")
print("\n",k, " (function) results found:")

loc_regex = re.compile(r"[^-]\d{3}-\d{3}-\d{4}")
int_regex = re.compile(r"\+1-\d{3}-\d{3}-\d{4}")
all_regex = re.compile(r"((\+1-)?\d{3}-\d{3}-\d{4})")

loc_results = re.findall(loc_regex, text)
int_results = re.findall(int_regex, text)
all_results = re.findall(all_regex, text)

print("\n",len(loc_results), " (regex) results found:")
for res in loc_results: print(res, end = ",  ")

print("\n\n",len(int_results), " (regex) int_results found:")
for res in int_results: print(res, end = ",  ")

print("\n\n",len(all_results), " (regex) all_results found:")
for res in all_results: print(res[0], end = ",  ")
